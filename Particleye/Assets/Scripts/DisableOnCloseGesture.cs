﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;


public class DisableOnCloseGesture : MonoBehaviour {

    public enum Side
    {
        Left,
        Right,
    }

    private Body _KinectBody;
    public GameObject Controlled;

    public Side Hand;

    // Use this for initialization
    void Start()
    {
        var name = transform.parent.name;
        Hand = name.EndsWith("Left") ? Side.Left : Side.Right;


        // attach the kinect body if its wanted
        var kinectBodyAttachment = GetComponent<KinectBody>();
        if (kinectBodyAttachment == null)
            Debug.LogError("AllignToKinectHand requires to also have a 'KinectBody-Script' attached");
       else
            _KinectBody = kinectBodyAttachment.Body;
    }

    // Update is called once per frame
    void Update()
    {
        if (_KinectBody != null)
        {
            var closed = false;
            if (Hand == Side.Left)
                closed = _KinectBody.HandLeftState == HandState.Closed && _KinectBody.HandLeftConfidence == TrackingConfidence.High;
            else
                closed = _KinectBody.HandRightState == HandState.Closed && _KinectBody.HandRightConfidence == TrackingConfidence.High;

            // toggle?
            if (Controlled.activeSelf == closed)
                Controlled.SetActive(!closed);
        }
    }
}
