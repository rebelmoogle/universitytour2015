﻿using UnityEngine;
using System.Collections;
using System;

public class GlowOnHit : BaseOnHit {

	public Material Material;
    private Color EmissionColor;
    private Color CurrentColor;

	// Use this for initialization
	void Start () {
        EmissionColor = Material.GetColor("_EmissionColor");
	}
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();

        CurrentColor.a = 1.0f;
        CurrentColor.r = EmissionColor.r * Strength;
        CurrentColor.g = EmissionColor.g * Strength;
        CurrentColor.b = EmissionColor.b * Strength;

        var renderer = GetComponent<UnityEngine.Renderer>();
        renderer.material.shader = Shader.Find("Standard");
        renderer.material.SetColor("_EmissionColor", CurrentColor);
	}
}
