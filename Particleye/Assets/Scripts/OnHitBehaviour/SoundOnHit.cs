﻿using UnityEngine;
using System.Collections;
using System;

public class SoundOnHit : BaseOnHit {

	public AudioSource Source;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	protected override void Update () {
		base.Update();
		Source.volume = Strength;
	}

}
