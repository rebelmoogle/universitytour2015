﻿using UnityEngine;
using System.Collections;
using System;

public class BaseOnHit : MonoBehaviour {

	public int MinHitCounter = 0;
	public int MaxHitCounter = 50000;
	public int CounterDecrement = 50;

	private int HitCounter = 0;

	private ParticleCollisionEvent[] collisionEvents = new ParticleCollisionEvent[0];

	protected float Strength;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	protected virtual void Update () {

		float strength = (float)(HitCounter - MinHitCounter) / (float)(MaxHitCounter - MinHitCounter);
        Strength = Math.Max(0, Math.Min(strength, 1));

		HitCounter = Math.Max(HitCounter - CounterDecrement, 0);
	}

	void OnParticleCollision(GameObject other)
	{
		var particleSystem = other.GetComponent<ParticleSystem>();

		var safeSize = UnityEngine.ParticlePhysicsExtensions.GetSafeCollisionEventSize(particleSystem);
		if(collisionEvents.Length < safeSize)
			collisionEvents = new ParticleCollisionEvent[safeSize];
		
		int hits = UnityEngine.ParticlePhysicsExtensions.GetCollisionEvents(particleSystem, gameObject, collisionEvents);
		HitCounter = Math.Min(HitCounter + hits, MaxHitCounter);

        Debug.Log(gameObject.name + ": " + Strength);
	}
}
