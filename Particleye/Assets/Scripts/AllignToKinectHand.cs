﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;
using System;

public class AllignToKinectHand : MonoBehaviour {

	private Transform _HandTip;
	private Transform _Wrist;
    private Transform _Thumb;
    private Transform _Elbow;

	/// <summary>
	/// Number of frames used for temporal smoothing.
	/// </summary>
	public float Smoothing = 5.0f;

    public bool UsePalm = false;


	private Quaternion _SmoothedRotation = Quaternion.identity;

	// Use this for initialization
	void Start () {

		var body = transform.parent.parent;
		var name = transform.parent.name;
		var side = name.EndsWith("Left") ? "Left" : "Right";

		_HandTip = body.FindChild("HandTip" + side);
		_Thumb = body.FindChild("Thumb" + side);
        _Wrist = body.FindChild("Wrist" + side);
        _Elbow = body.FindChild("Elbow" + side);
	}
	
	// Update is called once per frame
	void Update () {

		// allign to hand
		var tipDir = _HandTip.position - _Wrist.position;
		tipDir.Normalize();

		var thumbDir = _Thumb.position - _Wrist.position;
		thumbDir.Normalize();

		var targetDir = Vector3.Cross(thumbDir, tipDir);
		targetDir.Normalize();

        if(!UsePalm)
        {
            targetDir = _HandTip.position - _Wrist.position;
            targetDir.Normalize();
        }


		// smoothing
		Smoothing = Math.Max(1.0f, Smoothing);
		if (Smoothing < 2)
			transform.rotation = Quaternion.LookRotation(targetDir, Vector3.up);
		else
		{
			_SmoothedRotation = Quaternion.Lerp(
				Quaternion.LookRotation(targetDir, Vector3.up),
				_SmoothedRotation,
				1.0f / Smoothing);
			transform.rotation = _SmoothedRotation;
		}
	}
}
