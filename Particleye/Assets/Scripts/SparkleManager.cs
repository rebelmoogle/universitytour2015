﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SparkleManager : MonoBehaviour
{
    public BodySourceView BodySourceView;

    public GameObject SceneGraphParent;

    // "prototype"
    public GameObject LeftHand;
    public GameObject RightHand;

    // Use this for initialization
    void Start()
    {
        BodySourceView.BodyDiscovered += OnBodyDiscovered;
        BodySourceView.BodyLost += OnBodyLost;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnBodyDiscovered(object sender, BodyVisibilityArgs e)
    {
        Debug.Log("Discovered: " + e.Id);

        GameObject body = BodySourceView.GetBody(e.Id);

        if (SceneGraphParent != null)
        {
            body.transform.parent = SceneGraphParent.transform;
            body.transform.localPosition = Vector3.zero;
        }

        if (LeftHand != null)
        {
            GameObject clone = Instantiate(LeftHand);
            clone.transform.parent = body.transform.FindChild("HandLeft");
            clone.transform.localPosition = Vector3.zero;

            // attach the kinect body if its wanted
            var kinectBodyAttachment = clone.GetComponent<KinectBody>();
            if(kinectBodyAttachment != null)
                kinectBodyAttachment.Body = BodySourceView.GetKinectBody(e.Id); ;
        }

        if (RightHand != null)
        {
            GameObject clone = Instantiate(RightHand);
            clone.transform.parent = body.transform.FindChild("HandRight");
            clone.transform.localPosition = Vector3.zero;

            // attach the kinect body if its wanted
            var kinectBodyAttachment = clone.GetComponent<KinectBody>();
            if (kinectBodyAttachment != null)
                kinectBodyAttachment.Body = BodySourceView.GetKinectBody(e.Id); ;
        }
    }

    private void OnBodyLost(object sender, BodyVisibilityArgs e)
    {
        Debug.Log("Lost: " + e.Id);
    }
}
