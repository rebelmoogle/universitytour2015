﻿using UnityEngine;
using System.Collections;

public class SimpleMove : MonoBehaviour {

    public float moveSpeed = 10.0f;
    public float accelerationSpeed = 100.0f;

    Vector3 translateHorizontal;
    Vector3 translateVertical;

    Rigidbody mainObjectRigidBody;

	// Use this for initialization
	void Start () 
    {

        if (this.transform.root.CompareTag("Hand"))
        {
            HandInputHandler thisInputHandler = this.gameObject.GetComponent<HandInputHandler>();
            if (thisInputHandler != null)
            {
                thisInputHandler.MoveHorizontal += MoveHorizontalListener;
                thisInputHandler.MoveVertical += MoveVerticalListener;
                thisInputHandler.MovementRelease += MoveRelease;
            }
            
            mainObjectRigidBody = this.gameObject.GetComponent<Rigidbody>();
            if(mainObjectRigidBody == null)
            {
                mainObjectRigidBody = this.gameObject.AddComponent<Rigidbody>();
                mainObjectRigidBody.useGravity = false;
            }
        }	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    // cause Physics or someth.
    void FixedUpdate()
    {
        if(mainObjectRigidBody)
            mainObjectRigidBody.MovePosition(transform.position + ((translateHorizontal + translateVertical) * moveSpeed + (Vector3.forward * accelerationSpeed)) * Time.fixedDeltaTime);
    }

    void MoveHorizontalListener(GameObject e, float strength)
    {
        translateHorizontal = Vector3.forward * strength;
    }

    void MoveVerticalListener(GameObject e, float strength)
    {
        translateVertical = Vector3.right * strength;
    }

    void MoveRelease(GameObject e)
    {
        translateVertical = Vector3.zero;

        translateHorizontal = Vector3.zero;
    }
}
