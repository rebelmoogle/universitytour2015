﻿using UnityEngine;
using System.Collections;



public class HandInputHandler: MonoBehaviour {


    public delegate void InputHandler(GameObject e, float strength);
    public delegate void EventHandler(GameObject e);
    public event InputHandler MoveHorizontal;
    public event InputHandler MoveVertical;
    public event InputHandler CastSparkles;
    public event EventHandler CastRelease;
    public event InputHandler ShootSparkles;
    public event EventHandler MovementRelease;

    public float shootChargeTime = 1.0f;
    public float castThreshold = 0.01f;
    float previouscastVal = 0.0f;
    float currentCharge = 0.0f;
    bool previousMovement = false;
    bool isInverted = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        //get the input

        // ### normal movement ###
        bool noMovement = true;
        // handle it (tell the physics to move, the soundmanager to play a sound or the game to instantiate a LAZERBEAM)
        float axisVal = Input.GetAxis("Horizontal");
        if (Mathf.Abs(axisVal) >= 0.1)
        {
            OnMoveHorizontal(axisVal);
            noMovement = false;
        }

        axisVal = Input.GetAxis("Vertical");
        if (Mathf.Abs(axisVal) >= 0.1)
        {
            if (isInverted)
                axisVal = -axisVal;
            OnMoveVertical(axisVal);
            noMovement = false;
        }

        previousMovement = noMovement;

        if (noMovement && previousMovement)
            OnMovementRelease();

        /// ### Shoot Sparkles ###
                /// charge and shoot, go farther and can paint / light environment
        /// on trigger (axis)
        float shootAxisVal = Input.GetAxis("Shoot");
        if (shootAxisVal > castThreshold) // is pressing the fire button.
        {
            if (currentCharge < 1.0)
            {
                //increase 
                currentCharge += Time.deltaTime / shootChargeTime;
            }

        }
        else if (currentCharge > 0.0f) //button has been released and lazers are charged!
        {
            OnShoot(currentCharge);

            //reset
            currentCharge = 0.0f; // reset charge.
        }

        /// ### Cast Sparkles ###
        // continuously.
        // might change this later, but will work for now.
        float castAxisVal = Input.GetAxis("Cast");
        if (castAxisVal > castThreshold)
        {
            OnCast(castAxisVal);
            previouscastVal = castAxisVal;
        }
        else if(previouscastVal > castThreshold)
        {
            OnCastRelease();
            previouscastVal = castThreshold;
        }



        if (Input.GetButton("Inverted"))
        {
            isInverted = !isInverted;
        }
	
	}

    private void OnCastRelease()
    {
        if (CastRelease != null)
            CastRelease(this.gameObject);

    }

    private void OnShoot(float currentCharge)
    {
        if(ShootSparkles != null)
        {
            ShootSparkles(this.gameObject, Mathf.Min(currentCharge, 1.0f));
        }
    }

    void OnMoveHorizontal(float strength)
    {
        if (MoveHorizontal != null)
            MoveHorizontal(this.gameObject, strength);
    }

    void OnMoveVertical(float strength)
    {
        if (MoveVertical != null)
            MoveVertical(this.gameObject, strength);
    }

    void OnMovementRelease()
    {
        if (MovementRelease != null)
            MovementRelease(this.gameObject);
    }

    void OnCast(float strength)
    {
        if (CastSparkles != null)
            CastSparkles(this.gameObject, Mathf.Min(strength, 1.0f));
    }
}
