﻿using UnityEngine;
using System.Collections;

public class ShootSparklez : MonoBehaviour {

    public ParticleSystem castSparklez;
    ParticleSystem castClone;
    public ParticleSystem shootSparkles;
    ParticleSystem shootClone;

    public int maxCastEmissionRate = 500;
    public float maxCastStartSpeed = 2;

    public int maxShootParticles = 2000;
    public float maxShootStartSpeed = 10;

	// Use this for initialization
	void Start () 
    {
        HandInputHandler playerInput = this.gameObject.GetComponent<HandInputHandler>();
        if (playerInput != null)
        {
            playerInput.CastSparkles += player_CastSparkles;
            playerInput.CastRelease += player_CastRelease;
            playerInput.ShootSparkles += player_ShootSparkles;
        }

        //init particle system, attach them to the player. 
        castClone = Instantiate<ParticleSystem>(castSparklez);
        castClone.transform.parent = this.transform;
        castClone.transform.position = this.transform.position + transform.forward * 1.0f + transform.right * 0.5f;
        castClone.loop = true;
        castClone.playOnAwake = false;
        castClone.Stop();

        //init particle system, attach them to the player. 
        shootClone = Instantiate<ParticleSystem>(shootSparkles);
        shootClone.transform.parent = this.transform;
        shootClone.transform.position = this.transform.position + transform.forward * 1.0f + transform.right * -0.5f;
        shootClone.loop = false;
        shootClone.playOnAwake = false;
        shootClone.startDelay = 0.0f;
        shootClone.Stop();
    }

    private void player_ShootSparkles(GameObject e, float strength)
    {
        if (shootClone)
        {
            shootClone.Stop();
            shootClone.startSpeed = maxShootStartSpeed * ((strength * 0.5f) + 0.5f);
            shootClone.Play();
        }
    }


    private void player_CastSparkles(GameObject e, float strength)
    {
        if (castClone)
        {
            castClone.loop = true;
            castClone.emissionRate = maxCastEmissionRate * ((strength * 0.5f) + 0.5f);
            castClone.startSpeed = maxCastStartSpeed * ((strength * 0.5f) + 0.5f);
            if (castClone.isStopped || castClone.isPaused)
                castClone.Play();

            castClone.Emit(maxCastEmissionRate);
        }
    }

    
    private void player_CastRelease(GameObject e)
    {
        castClone.loop = false;
    }
	
	// Update is called once per frame
	void Update () 
    {
	    
	}
}
